//
//  careers.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 27/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation


struct Careers : Decodable {
    
    let id : Int?
    let title : String?
    let description : String?
    
}
