//
//  GlobalVars.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 05/11/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
import UIKit

//Declare variable to cache Newsfeed images.
var postImageCache = NSCache<NSString, UIImage>()

//extension to download images from internet
extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        contentMode = mode
        //make sure the cell is emptied before checking cache for  image
        image = nil
        //check if image is in cache before loading it from the internet
        if let imageFromCache = postImageCache.object(forKey: url.absoluteString as NSString){
            
            self.image = imageFromCache
            return
        }
        
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                
                let imageToCache = image
                
                postImageCache.setObject(imageToCache, forKey: url.absoluteString as NSString)
                
                self.image = imageToCache
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}


var accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImUzYzRkNzYyMWM0NDE2ZTU2MGNkNTg5Yzk2OTM5YWRjZjU5NjliNzc5YjUyNDhkOWI3MTI3YjgzMWE0MmE0ZTI0NjVjOThhYWRlMGIwMGE3In0.eyJhdWQiOiIxIiwianRpIjoiZTNjNGQ3NjIxYzQ0MTZlNTYwY2Q1ODljOTY5MzlhZGNmNTk2OWI3NzliNTI0OGQ5YjcxMjdiODMxYTQyYTRlMjQ2NWM5OGFhZGUwYjAwYTciLCJpYXQiOjE1MjE4MzAwNzQsIm5iZiI6MTUyMTgzMDA3NCwiZXhwIjoxNTUzMzY2MDc0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bBvwMniR96uLQfRDrc1E3uDU8_nvKyVHlKuvGjicyYJH3WmmCYDtRT5clL28g-Zm2SoYAAT8m9oAGmKiGSlL5wtsmtHxXcxNCRCy9EbHINCyVidKuGHWiD4Wb5tcdBCxCIbgnXiot-QIT4HR4njBxE_IHghtAXLXBh4mbMOdHR59wrK7e_4jbwYqPI0CaVwte_T6V--vh09DiIaawHD-Jganj53muSqZxspfaw5zqVsnlcQG2jbe4hDpPvPRs0xCzxb-y8GJzTk46Y7vb1ttMkHdpx6MuCzoKpMjmrLWax-ORWswqXFYMJTiXdSxXUn-7KPhqUX9WAlQL9vbxJHZDV_Ja_QSPNzC-NB5UzpL1TbR_h_h6ABTUNk9mYeGKPYdfOG_osu8AEh5c0QDIjEk86OXnrWydEAX2e0JfzRbbp-0TRB_xOkcamWmRY1qldsqXyKgf-upIDj9sv9wCY5OZNTfRvQEWNYg1Uz-hfFHd8KOglPWLNkJNw9chzL7MqDAMv-0dICwHil76DyW5KKWDhD5sCxwLkaPBs8DRolcTVa6bWoDO06tjS7lTUuEJS0NR4WxQyuttdYUypJAzTd_Rwz_F9zAuJOddW2qLp9USBbD3GN_wFsYj9oIlx6uZA84yZCSHwjByfaeKEm5JC0ktQYMIQQckGhUiiNmKqYEko8"

var newsTitle : String?
var newsPhotoUrl : String?
var newsDescription : String?
var titleViewController = UILabel()

var sideMenu = SlideMenuTableViewController.instantiateFromStoryboardArticles(UIStoryboard(name: "Main", bundle: nil))
let blackView = UIView()
var gestureLeft = UIScreenEdgePanGestureRecognizer()
var gestureRight = UIPanGestureRecognizer()


