//
//  EndPoint.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 21/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
protocol Endpoint {
    
    var base: String { get }
    var path: String { get }
}

extension Endpoint {

    var bearerString: String {
        return "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImUzYzRkNzYyMWM0NDE2ZTU2MGNkNTg5Yzk2OTM5YWRjZjU5NjliNzc5YjUyNDhkOWI3MTI3YjgzMWE0MmE0ZTI0NjVjOThhYWRlMGIwMGE3In0.eyJhdWQiOiIxIiwianRpIjoiZTNjNGQ3NjIxYzQ0MTZlNTYwY2Q1ODljOTY5MzlhZGNmNTk2OWI3NzliNTI0OGQ5YjcxMjdiODMxYTQyYTRlMjQ2NWM5OGFhZGUwYjAwYTciLCJpYXQiOjE1MjE4MzAwNzQsIm5iZiI6MTUyMTgzMDA3NCwiZXhwIjoxNTUzMzY2MDc0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bBvwMniR96uLQfRDrc1E3uDU8_nvKyVHlKuvGjicyYJH3WmmCYDtRT5clL28g-Zm2SoYAAT8m9oAGmKiGSlL5wtsmtHxXcxNCRCy9EbHINCyVidKuGHWiD4Wb5tcdBCxCIbgnXiot-QIT4HR4njBxE_IHghtAXLXBh4mbMOdHR59wrK7e_4jbwYqPI0CaVwte_T6V--vh09DiIaawHD-Jganj53muSqZxspfaw5zqVsnlcQG2jbe4hDpPvPRs0xCzxb-y8GJzTk46Y7vb1ttMkHdpx6MuCzoKpMjmrLWax-ORWswqXFYMJTiXdSxXUn-7KPhqUX9WAlQL9vbxJHZDV_Ja_QSPNzC-NB5UzpL1TbR_h_h6ABTUNk9mYeGKPYdfOG_osu8AEh5c0QDIjEk86OXnrWydEAX2e0JfzRbbp-0TRB_xOkcamWmRY1qldsqXyKgf-upIDj9sv9wCY5OZNTfRvQEWNYg1Uz-hfFHd8KOglPWLNkJNw9chzL7MqDAMv-0dICwHil76DyW5KKWDhD5sCxwLkaPBs8DRolcTVa6bWoDO06tjS7lTUuEJS0NR4WxQyuttdYUypJAzTd_Rwz_F9zAuJOddW2qLp9USBbD3GN_wFsYj9oIlx6uZA84yZCSHwjByfaeKEm5JC0ktQYMIQQckGhUiiNmKqYEko8"
    }
    
    var urlComponents: URLComponents {
        var components = URLComponents(string: base)!
        components.path = path
//        components.query = apiKey
        return components
    }
    
    var headers: String{
        
        let header = bearerString
        
        return header
    }
    
    var request: URLRequest {
    
//        request.setValue(bearerString, forHTTPHeaderField: "Authorization")
        let url = urlComponents.url!
        
        var requests = URLRequest(url: url)
        
        requests.setValue(bearerString, forHTTPHeaderField: "Authorization")
        
        return requests
    }
    
    
}

enum EacFeed {
    
    case news
    case careers
    case events
}

extension EacFeed: Endpoint {
    
    var base: String {
        return "https://eacmobileapp.eac.int"
    }
    
    var path: String {
        switch self {
        case .news: return "/public/api/news"
        case .careers: return "/public/api/careers"
        case .events: return "/public/api/events"
        }
    }
}
