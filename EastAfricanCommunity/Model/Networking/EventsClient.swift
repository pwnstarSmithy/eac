//
//  EventsClient.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 22/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
class EventsClient: APIClient {
    
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    //in the signature of the function in the success case we define the Class type thats is the generic one in the API
    func getEvents(from eventsFeedType: EacFeed, completion: @escaping (Result<[Events]?, APIError>) -> Void) {
        
        let endpoint = eventsFeedType
        let request = endpoint.request
        
        fetch(with: request, decode: { json -> [Events]? in
            guard let eventsResult = json as? [Events] else { return  nil }
            return eventsResult
        }, completion: completion)
    }
    
}
