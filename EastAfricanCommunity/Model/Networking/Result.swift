//
//  Result.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 21/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

enum Result<T, U> where U: Error  {
    case success(T)
    case failure(U)
}
