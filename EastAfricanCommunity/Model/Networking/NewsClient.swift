//
//  NewsClient.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 21/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

class NewsClient: APIClient {
    
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
       //in the signature of the function in the success case we define the Class type thats is the generic one in the API
    func getNews(from newsFeedType: EacFeed, completion: @escaping (Result<[News]?, APIError>) -> Void) {
    
            let endpoint = newsFeedType
            let request = endpoint.request
    
            fetch(with: request, decode: { json -> [News]? in
                guard let newsResult = json as? [News] else { return  nil }
                return newsResult
            }, completion: completion)
   }

}
