//
//  CareersClient.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 21/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

class CareersClient: APIClient {
    
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    //in the signature of the function in the success case we define the Class type thats is the generic one in the API
    func getCareers(from careersFeedType: EacFeed, completion: @escaping (Result<[Careers]?, APIError>) -> Void) {
        
        let endpoint = careersFeedType
        let request = endpoint.request
        
        fetch(with: request, decode: { json -> [Careers]? in
            guard let careersResult = json as? [Careers] else { return  nil }
            return careersResult
        }, completion: completion)
    }
    
}
