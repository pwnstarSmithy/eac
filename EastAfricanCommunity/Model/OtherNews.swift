//
//  OtherNews.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 28/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

struct OtherNews {
    let title : String?
    let link : String?
    let description : String?

}

class FeedParser: NSObject, XMLParserDelegate
{
    
    private var rssItems: [OtherNews] = []
    private var currentElement = ""
    private var currentTitle: String = "" {
        
        didSet {
            currentTitle = currentTitle.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        
    }
    private var currentLink: String = "" {
        
        didSet {
            
            currentLink = currentLink.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
        }
        
    }
    private var currentDescription: String = ""{
        
        didSet{
            
            currentDescription = currentDescription.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
        }
        
    }
   
    private var parserCompletionHandler: (([OtherNews]) -> Void)?
    
    func parseFeed(url: String, completionHandler:(([OtherNews]) -> Void)?)
    {
        
    self.parserCompletionHandler = completionHandler
        
        let request = URLRequest(url: URL(string: url)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {
                
                if let error = error {
                    
                    print(error.localizedDescription)
                    
                }
                return
            }
            
            //parse our xml data
            
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
            
        }
        task.resume()
        
    }
    
    
    //MARK: - XML Parser Delegate
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        
        currentElement = elementName
        if currentElement == "item"{
            
            currentTitle = ""
            currentLink = ""
            currentDescription = ""
            
            
        }
 
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        switch currentElement {
            
        case "title": currentTitle += string
        case "description": currentDescription += string
        case "link": currentLink += string
        default: break
            
        }
        
        
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "item"{
            
            let rssItem = OtherNews(title: currentTitle, link: currentLink, description: currentTitle)
            self.rssItems.append(rssItem)
            
        }
        
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
      
        parserCompletionHandler?(rssItems)
        
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError.localizedDescription)
    }
}





