//
//  News.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 24/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

//struct News : Decodable {
//    let newsArray : [NewsArray]?
//}
struct News : Decodable {
    let id : Int?
    let title : String?
    let details : String?
    let photos : [PhotosArray]?
}
struct PhotosArray : Decodable {
    let id : Int?
    let news_id : String?
    let name : String?
}
