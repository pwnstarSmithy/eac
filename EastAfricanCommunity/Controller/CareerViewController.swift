//
//  CareerViewController.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 27/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class CareerViewController: UIViewController {

    @IBOutlet weak var titleSelected: UILabel!
    
    @IBOutlet weak var descriptionSelected: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleSelected.text = careerTitle
        
        let cutDescription = careerDesciption?.replacingOccurrences(of: "<p>", with: "")
        let moreCutDescription = cutDescription?.replacingOccurrences(of: "</p>", with: "")
        let x = moreCutDescription?.replacingOccurrences(of: "<strong>", with: "")
        let y = x?.replacingOccurrences(of: "</strong>", with: "")
        let z = y?.replacingOccurrences(of: "&rsquo;", with: "")
        let r = z?.replacingOccurrences(of: "&ldquo;", with: "")
        let g = r?.replacingOccurrences(of: "&rdquo;", with: "")
        let finalReplace = g?.replacingOccurrences(of: "&nbsp;", with: "")
        
        
        descriptionSelected.text = finalReplace
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
