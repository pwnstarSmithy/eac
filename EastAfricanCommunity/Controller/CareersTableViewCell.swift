//
//  CareersTableViewCell.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 27/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class CareersTableViewCell: UITableViewCell {

    @IBOutlet weak var careersTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
