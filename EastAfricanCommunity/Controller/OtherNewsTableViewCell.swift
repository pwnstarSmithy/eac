//
//  OtherNewsTableViewCell.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 28/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class OtherNewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsTitle: UILabel!
    
    @IBOutlet weak var newsDescription: UILabel!
    
    
    var item: OtherNews! {
        didSet {
            
            newsTitle.text = item.title
            newsDescription.text = item.description
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
