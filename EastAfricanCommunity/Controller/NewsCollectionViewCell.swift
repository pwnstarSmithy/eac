//
//  NewsCollectionViewCell.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 24/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var newsImage: UIImageView!
    
    @IBOutlet weak var newsCaption: UILabel!
    
}
