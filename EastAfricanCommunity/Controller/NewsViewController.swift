//
//  NewsViewController.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 30/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import WebKit

class NewsViewController: UIViewController, WKUIDelegate, WKNavigationDelegate{

    @IBOutlet weak var showNews: WKWebView!
    
    override func loadView() {
        
        showNews = WKWebView()
        showNews.navigationDelegate = self
        view = showNews
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myURL = URL(string: articleLink!)
        let myRequest = URLRequest(url: myURL!)
        showNews.load(myRequest)
   
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
