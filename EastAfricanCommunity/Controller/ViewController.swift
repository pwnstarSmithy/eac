//
//  ViewController.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 24/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import SwiftSpinner



class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

//    var newsArray = [NewsArray]()

    var newsArray = [News]()
    
    private let newsClient = NewsClient()
    
    @IBAction func presentMenu(_ sender: Any) {
        setLeftMenu()
    }
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.setGestureRecognizers()
        titleViewController.text = "East African News"
        
        setNavigationController()
        //set navigation bar to no color
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        //set line under navigation bar to 0
        navigationController?.navigationBar.shadowImage = UIImage()
        
        //remove back button words
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5)
        layout.minimumInteritemSpacing = 1
        layout.itemSize = CGSize(width: (self.collectionView.frame.size.width - 20)/1, height: self.collectionView.frame.size.height/3)
        
        
        collectionView.dataSource = self
        collectionView.delegate = self
      
        tryNews()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //Set the Navigation View Controller
    func setNavigationController() {
        
        self.navigationItem.titleView = nil
        titleViewController.removeFromSuperview()
        //Set the title of the View Controller.
        titleViewController.frame = CGRect(x: 65, y: 6, width: self.view.frame.width - 150, height: 34)
        titleViewController.font = UIFont(name: "Roboto-Bold" , size: 23)
        titleViewController.textColor = UIColor.black
       titleViewController.textAlignment = .left
        self.navigationController?.navigationBar.addSubview(titleViewController)
        //Set navigation bar.
        self.navigationController?.navigationBar.barTintColor =  UIColor(red:119, green: 136, blue: 153, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(color:UIColor(red:119, green: 136, blue: 153, alpha: 1)), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        DispatchQueue.main.async {
        self.handleDismiss()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tryNews(){
        
        SwiftSpinner.show("Loading data")
        newsClient.getNews(from: .news) { result in
            switch result {
            case .success(let newsResult):
//                guard let newsArrayResults = [News?].self else {return}
                
                guard let newsArrayResults = newsResult else {return}
                
                self.newsArray = newsArrayResults
                
                for lugambo in newsArrayResults {
                
                }
                DispatchQueue.main.async(execute: {
                    
                    self.collectionView.reloadData()
                })
                SwiftSpinner.hide()
            case .failure(let error):
                SwiftSpinner.hide()
                print("the error \(error)")
                
            }
            
        }

    }
    //Set gesture recognizers
    func setGestureRecognizers() {
        gestureLeft = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(ViewController.handleTap(_:)))
        gestureLeft.edges = .left
        self.view.addGestureRecognizer(gestureLeft)
        gestureRight = UIPanGestureRecognizer(target: self, action: #selector(handleDragToLeft))
        blackView.addGestureRecognizer(gestureRight)
        blackView.alpha = 0
    }
    //MARK: - Side Menu
    func setLeftMenu(){
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            sideMenu.view.backgroundColor = UIColor(white: 1, alpha: 1)
            window.addSubview(blackView)
            window.addSubview(sideMenu.view)
            sideMenu.view.frame = CGRect(x: 0, y: 0, width: 0, height: window.frame.height)
            blackView.frame = window.frame
            blackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                sideMenu.view.frame = CGRect(x: 0, y: 0, width: window.frame.width - 70, height: window.frame.height)
                blackView.alpha = 1
            }, completion: nil)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
        }
    }
    
    //This function reveals the left menu.
    func showMenu(_ sender: UIBarButtonItem) {
        self.setLeftMenu()
    }
    
    //This function reveals the left menu when dragged from left.
    @objc func handleTap(_ gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            gestureRecognizer.isEnabled = false
            self.setLeftMenu()
            gestureRecognizer.isEnabled = true
        }
    }

    //This function handles the dismiss of the left menu.
    @objc func handleDragToLeft(_ sender: UIPanGestureRecognizer) {
        handleDismiss()
    }

    func loadNews() {
        
        SwiftSpinner.show("Loading data")
        
        let url = NSURL(string: "https://eacmobileapp.eac.int/public/api/news")!
        
        let tokenString = "Bearer " + accessToken
        
        var request = URLRequest(url: url as URL)

        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}

            if error == nil {
                
                do {
                    
                    let pulledData = try JSONDecoder().decode([News].self, from: data)
                    
                    self.newsArray = pulledData
                    
                    for processedNews in self.newsArray{
            
                    }

                    DispatchQueue.main.async(execute: {
                        
                        self.collectionView.reloadData()
                    })
                     SwiftSpinner.hide()
                }catch{
                     SwiftSpinner.hide()
                    print(error)
                    
                }
                
                
            }else{
                 SwiftSpinner.hide()
                print(error!)
                
            }
            
            }.resume()
      
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return newsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath) as! NewsCollectionViewCell
        
        cell.newsCaption.text = newsArray[indexPath.item].title
        
        let baseUrl = "https://eacmobileapp.eac.int/uploads/"
        
        let photoArray = newsArray[indexPath.item].photos?.first
        
        let imageName = photoArray?.name
        
        let finalUrl = "\(baseUrl)\(imageName!)"
        
        let photoUrl = URL(string: finalUrl)
  
        cell.newsImage.downloadedFrom(url: photoUrl!)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        newsTitle = newsArray[indexPath.item].title
        
        let photoArray = newsArray[indexPath.item].photos?.first
        
        newsPhotoUrl = photoArray?.name
        
        newsDescription = newsArray[indexPath.item].details
        
    }
    
}


extension ViewController {
    //MARK: Handle sideMenu dismiss
    @objc public func handleDismiss() {
        UIView.animate(withDuration: 0.5, animations: {
            blackView.alpha = 0
            if  UIApplication.shared.keyWindow != nil {
                sideMenu.view.frame = CGRect(x: 0, y: 1, width: 0, height: sideMenu.view.frame.height)
            }
        })
    }
    
    
}












