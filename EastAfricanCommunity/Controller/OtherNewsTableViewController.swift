//
//  OtherNewsTableViewController.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 28/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import SwiftSpinner

var articleLink : String?

class OtherNewsTableViewController: UITableViewController {
    
    private var rssItems: [OtherNews]?
    
    @IBAction func showMenu(_ sender: Any) {
        
        setLeftMenu()
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setGestureRecognizers()
        fetchData()
        
        titleViewController.text = "Other News"
        setNavigationController()
        
        //set navigation bar to no color
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        //set line under navigation bar to 0
        navigationController?.navigationBar.shadowImage = UIImage()
        
        //remove back button words
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        handleDismiss()
    }
    //Set the Navigation View Controller
    func setNavigationController() {
        
        self.navigationItem.titleView = nil
        titleViewController.removeFromSuperview()
        //Set the title of the View Controller.
        titleViewController.frame = CGRect(x: 65, y: 6, width: self.view.frame.width - 150, height: 34)
        titleViewController.font = UIFont(name: "Roboto-Bold" , size: 23)
        titleViewController.textColor = UIColor.black
        titleViewController.textAlignment = .left
        self.navigationController?.navigationBar.addSubview(titleViewController)
        //Set navigation bar.
        self.navigationController?.navigationBar.barTintColor =  UIColor(red:119, green: 136, blue: 153, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(color:UIColor(red:119, green: 136, blue: 153, alpha: 1)), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
    }
    //Set gesture recognizers
    func setGestureRecognizers() {
        gestureLeft = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(ViewController.handleTap(_:)))
        gestureLeft.edges = .left
        self.view.addGestureRecognizer(gestureLeft)
        gestureRight = UIPanGestureRecognizer(target: self, action: #selector(handleDragToLeft))
        blackView.addGestureRecognizer(gestureRight)
        blackView.alpha = 0
    }
    //MARK: - Side Menu
    func setLeftMenu(){
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            sideMenu.view.backgroundColor = UIColor(white: 1, alpha: 1)
            window.addSubview(blackView)
            window.addSubview(sideMenu.view)
            sideMenu.view.frame = CGRect(x: 0, y: 0, width: 0, height: window.frame.height)
            blackView.frame = window.frame
            blackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                sideMenu.view.frame = CGRect(x: 0, y: 0, width: window.frame.width - 70, height: window.frame.height)
                blackView.alpha = 1
            }, completion: nil)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
        }
    }
    
    //This function reveals the left menu.
    func showMenu(_ sender: UIBarButtonItem) {
        self.setLeftMenu()
    }
    
    //This function reveals the left menu when dragged from left.
    @objc func handleTap(_ gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            gestureRecognizer.isEnabled = false
            self.setLeftMenu()
            gestureRecognizer.isEnabled = true
        }
    }
    
    //This function handles the dismiss of the left menu.
    @objc func handleDragToLeft(_ sender: UIPanGestureRecognizer) {
        handleDismiss()
    }
    //MARK: Handle sideMenu dismiss
    @objc public func handleDismiss() {
        UIView.animate(withDuration: 0.5, animations: {
            blackView.alpha = 0
            if  UIApplication.shared.keyWindow != nil {
                sideMenu.view.frame = CGRect(x: 0, y: 1, width: 0, height: sideMenu.view.frame.height)
            }
        })
    }
    private func fetchData(){
        
        SwiftSpinner.show("Loading data")
        
        let feedParser = FeedParser()
        print("XML data fetched!")
        feedParser.parseFeed(url: "https://theeastafrican.co.ke/2456-2456-view-asFeed-ee50ec/index.xml") { (rssItems) in
            self.rssItems = rssItems
            
            OperationQueue.main.addOperation {
                self.tableView.reloadSections( IndexSet(integer: 0), with: .left)
                SwiftSpinner.hide()
            }
            
        }
        
    }
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard let rssItems = rssItems else {
            
            return 0
            
        }
        return rssItems.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "otherNews", for: indexPath) as! OtherNewsTableViewCell

        if let item = rssItems?[indexPath.item]{
            
            cell.item = item
            
        }

        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        articleLink = rssItems?[indexPath.item].link
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
