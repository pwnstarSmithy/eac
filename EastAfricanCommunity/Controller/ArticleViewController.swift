//
//  ArticleViewController.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 24/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var articleTitle: UILabel!

    @IBOutlet weak var articleDesc: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        articleTitle.text = newsTitle
        
        let cutDescription = newsDescription?.replacingOccurrences(of: "<p>", with: "")
        let moreCutDescription = cutDescription?.replacingOccurrences(of: "</p>", with: "")
        let x = moreCutDescription?.replacingOccurrences(of: "<strong>", with: "")
        let y = x?.replacingOccurrences(of: "</strong>", with: "")
        let z = y?.replacingOccurrences(of: "&rsquo;", with: "")
        let r = z?.replacingOccurrences(of: "&ldquo;", with: "")
        let g = r?.replacingOccurrences(of: "&rdquo;", with: "")
        let finalReplace = g?.replacingOccurrences(of: "&nbsp;", with: "")
    
        articleDesc.text = finalReplace
        let baseUrl = "https://eacmobileapp.eac.int/uploads/"

        let finalUrl = "\(baseUrl)\(newsPhotoUrl!)"
        
        let photoUrl = URL(string: finalUrl)

        
        imageView.downloadedFrom(url: photoUrl!)

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
