//
//  AnthemViewController.swift
//  EastAfricanCommunity
//
//  Created by pwnstarSmithy on 25/09/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import AVFoundation

class AnthemViewController: UIViewController {
    
    var audioPlayer = AVAudioPlayer()
    
    
    @IBAction func showMenu(_ sender: Any) {
        
        setLeftMenu()
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setGestureRecognizers()
        
        titleViewController.text = "EAC Anthem"
        setNavigationController()
        
        //set navigation bar to no color
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        //set line under navigation bar to 0
        navigationController?.navigationBar.shadowImage = UIImage()
        
        //remove back button words
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        

        do{
            audioPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "anthem", ofType: "aac")!))
            audioPlayer.prepareToPlay()
            
        }catch{
            print(error)
        }
        
        // Do any additional setup after loading the view.
    }
    func setNavigationController() {
        
        self.navigationItem.titleView = nil
        titleViewController.removeFromSuperview()
        //Set the title of the View Controller.
        titleViewController.frame = CGRect(x: 65, y: 6, width: self.view.frame.width - 150, height: 34)
        titleViewController.font = UIFont(name: "Roboto-Bold" , size: 23)
        titleViewController.textColor = UIColor.black
        titleViewController.textAlignment = .left
        self.navigationController?.navigationBar.addSubview(titleViewController)
        //Set navigation bar.
        self.navigationController?.navigationBar.barTintColor =  UIColor(red:119, green: 136, blue: 153, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(color:UIColor(red:119, green: 136, blue: 153, alpha: 1)), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        handleDismiss()
    }
    @IBAction func playButton(_ sender: Any) {
        
        audioPlayer.play()
    }
    
    
    @IBAction func pauseButton(_ sender: Any) {
        
        if audioPlayer.isPlaying {
            
             audioPlayer.pause()
            
        }else{
            
        }
        
       
        
    }
    
    @IBAction func restartButton(_ sender: Any) {
        if audioPlayer.isPlaying {
            
            audioPlayer.currentTime = 0
            audioPlayer.play()
            
        }else{
            audioPlayer.play()
        }
    }
    //Set gesture recognizers
    func setGestureRecognizers() {
        gestureLeft = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(ViewController.handleTap(_:)))
        gestureLeft.edges = .left
        self.view.addGestureRecognizer(gestureLeft)
        gestureRight = UIPanGestureRecognizer(target: self, action: #selector(handleDragToLeft))
        blackView.addGestureRecognizer(gestureRight)
        blackView.alpha = 0
    }
    //MARK: - Side Menu
    func setLeftMenu(){
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            sideMenu.view.backgroundColor = UIColor(white: 1, alpha: 1)
            window.addSubview(blackView)
            window.addSubview(sideMenu.view)
            sideMenu.view.frame = CGRect(x: 0, y: 0, width: 0, height: window.frame.height)
            blackView.frame = window.frame
            blackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                sideMenu.view.frame = CGRect(x: 0, y: 0, width: window.frame.width - 70, height: window.frame.height)
                blackView.alpha = 1
            }, completion: nil)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
        }
    }
    
    //This function reveals the left menu.
    func showMenu(_ sender: UIBarButtonItem) {
        self.setLeftMenu()
    }
    
    //This function reveals the left menu when dragged from left.
    @objc func handleTap(_ gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            gestureRecognizer.isEnabled = false
            self.setLeftMenu()
            gestureRecognizer.isEnabled = true
        }
    }
    
    //This function handles the dismiss of the left menu.
    @objc func handleDragToLeft(_ sender: UIPanGestureRecognizer) {
        handleDismiss()
    }
    //MARK: Handle sideMenu dismiss
    @objc public func handleDismiss() {
        UIView.animate(withDuration: 0.5, animations: {
            blackView.alpha = 0
            if  UIApplication.shared.keyWindow != nil {
                sideMenu.view.frame = CGRect(x: 0, y: 1, width: 0, height: sideMenu.view.frame.height)
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
